import sys

articulos = {}

def anadir(articulo, precio):
    articulos[articulo] = precio

def mostrar():
    print("Lista de artículos en la tienda:")
    for articulo, precio in articulos.items():
        print(f"{articulo}: {precio} euros")

def pedir_articulo() -> str:
    while True:
        articulo = input("Artículo: ")
        if articulo in articulos:
            return articulo
        else:
            print("El artículo no está en la lista. Inténtalo de nuevo.")

def pedir_cantidad() -> float:
    while True:
        cantidad_str = input("Cantidad: ")
        try:
            cantidad = float(cantidad_str)
            return cantidad
        except ValueError:
            print("Cantidad no válida. Inténtalo de nuevo.")

def main():
    if len(sys.argv) < 3 or len(sys.argv) % 2 != 1:
        print("Error en argumentos: Debes proporcionar al menos un artículo y su precio.")
        return

    for i in range(1, len(sys.argv), 2):
        articulo = sys.argv[i]
        precio_str = sys.argv[i + 1]

        try:
            precio = float(precio_str)
            anadir(articulo, precio)
        except ValueError:
            print(f"Error en argumentos: {precio_str} no es un precio correcto para {articulo}.")
            return

    mostrar()

    seleccionado = pedir_articulo()
    cantidad = pedir_cantidad()
    precio_unitario = articulos[seleccionado]
    precio_total = cantidad * precio_unitario

    print(f"Compra total: {cantidad} de {seleccionado}, a pagar {precio_total} euros.")

if __name__ == '__main__':
    main()
